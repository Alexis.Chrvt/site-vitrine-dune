// _____     __  __     __   __     ______    
// /\  __-.  /\ \/\ \   /\ "-.\ \   /\  ___\   
// \ \ \/\ \ \ \ \_\ \  \ \ \-.  \  \ \  __\   
//  \ \____-  \ \_____\  \ \_\\"\_\  \ \_____\ 
//   \/____/   \/_____/   \/_/ \/_/   \/_____/ 
                                            

               

//menu

// let button = document.querySelector('svg');
// let menu = document.querySelector('ul');
// let menu_burger = false; 

// button.addEventListener('click',()=>{
//   if (menu_burger == false){
//     menu_burger = true;
//     menu.style.display = 'flex';
//   }
//   else {
//     menu_burger = false;  
//     menu.style.display = 'none';
//   }
// }); 

let toggleButton = document.querySelector('svg');
// console.log(toggleButton);

toggleButton.addEventListener('click', () => {
  aside.classList.toggle('show');
});

toggleButton.addEventListener('click', () => {
  aside.classList.toggle('hidden');
});


// API DE YOUTUBE 
// Obligatoire pour Codesandbox (pb de dépendances...)
// Le fichier script.js est donc ici inutile...
// Le script est placé à la fin du body car l'attribut defer ne fonctionne que pour des scripts externes
// CE N'EST PAS UNE BONNE PRATIQUE !!!

// Variable globale contenant l'état du lecteur
let etatLecteur;

function lecteurPret(event) {
  // event.target = lecteur
  event.target.setVolume(30);
}

function changementLecteur(event) {
  // event.data = état du lecteur
  etatLecteur = event.data;
}

let lecteur;

function onYouTubeIframeAPIReady() {
  lecteur = new YT.Player("video", {
    height: "390",
    width: "640",
    videoId: "n9xhJrPXop4",
    playerVars: {
      color: "white",
      enablejsapi: 1,
      modestbranding: 1,
      rel: 0
    },
    events: {
      onReady: lecteurPret,
      onStateChange: changementLecteur
    }
  });
}

// Hauteur de la vidéo
const hauteurVideo = $("#video").height();
// Position Y de la vidéo
const posYVideo = $("#video").offset().top;
// Valeur declenchant la modification de l'affichage (choix "esthétique")
const seuil = posYVideo + 0.75 * hauteurVideo;

// Gestion du défilement
$(window).scroll(function () {
  // Récupération de la valeur du défilement vertical
  const scroll = $(window).scrollTop();

  // Classe permettant l'exécution du CSS
  $("#video").toggleClass(
    "scroll",
    etatLecteur === YT.PlayerState.PLAYING && scroll > seuil
  );
});


// fonctionnement du carroussel 
// Variable globale
let index = 0;
$('.character>p').text($('.imagecar').eq(0).attr('alt'));

// Gestion des événements
$('.arrowR>.arrowp').click(function () {
  // Récupération index
  let indexN = index + 1;
  if (indexN > $('.carroussel>img').length - 1) {
    indexN = 0;
  }

  // Renouveller l'image
  $('.imagecar').eq(index).fadeOut(500).end().eq(indexN).fadeIn(500);
  $('.character>p').text($('.imagecar').eq(indexN).attr('alt'));
  index = indexN
});

$('.arrowL>.arrowp').click(function () {

  // Récupération index
  let indexN = index - 1;
  if (indexN < 0) {
    indexN = $('.carroussel>img').length - 1;
  }

  // Renouveller l'image
  $('.imagecar').eq(index).fadeOut(500).end().eq(indexN).fadeIn(500);
  $('.character>p').text($('.imagecar').eq(indexN).attr('alt'));
  index = indexN
});


//carroussel numéro 2
let index2 = 0;

// Gestion des événements
$('.arrowR2>.arrowp2').click(function () {
  // Récupération index
  let indexN = index2 + 1;
  if (indexN > $('.carroussel2>img').length - 1) {
    indexN = 0;
  }

  // Renouveller l'image
  $('.imagecar2').eq(index2).fadeOut(500).end().eq(indexN).fadeIn(500);
  index2 = indexN
});

$('.arrowL2>.arrowp2').click(function () {

  // Récupération index
  let indexN = index2 - 1;
  if (indexN < 0) {
    indexN = $('.carroussel2>img').length - 1;
  }

  // Renouveller l'image
  $('.imagecar2').eq(index2).fadeOut(500).end().eq(indexN).fadeIn(500);
  index2 = indexN
});


//parallaxe 

$('.scrollerhtml').on('scroll', function(){
  let windowPosition = $('.scrollerhtml').scrollTop();
  const speedFactor = 0.2;
  $('section').each(function(index){
      let elementPosition = (index)*window.innerHeight;
      let diff = elementPosition - windowPosition;
      let offset = diff * speedFactor;             
$(this).css('background-position-y', `${offset}px`);
  })
  return this;
})


//afficher la carte


// Création de la carte, vide à ce stade
let carte = L.map('carte', {
  center: [47.2608333, 2.4188888888888886], // Centre de la France
  zoom: 5,
  minZoom: 4,
  maxZoom: 18,
});

// Ajout des tuiles (ici OpenStreetMap)
// https://wiki.openstreetmap.org/wiki/Tiles#Servers
L.tileLayer('https://a.tile.openstreetmap.org/{z}/{x}/{y}.png', {
  attribution: '&copy; <a href="https://openstreetmap.org/copyright">OpenStreetMap contributors</a>',
}).addTo(carte);

// Ajout de l'échelle
L.control.scale().addTo(carte);


//récupération de coordonnée GPS
// Récupération des coordonnées au clic sur le bouton
$("span").click(function () {
  // Support de la géolocalisation
  if ("geolocation" in navigator) {
    // Support = exécution du callback selon le résultat
    navigator.geolocation.getCurrentPosition(positionSucces, positionErreur, {
      enableHighAccuracy: true,
      timeout: 5000,
      maximumAge: 30000
    });
  } else {
    // Non support = injection de texte
    $(".erreur").text("La géolocalisation n'est pas supportée par votre navigateur");
  }
});

// Appelée si récupération des coordonnées réussie
function positionSucces(position) {
  // Injection du résultat dans du texte
  const lat = Math.round(1000 * position.coords.latitude) / 1000;
  const long = Math.round(1000 * position.coords.longitude) / 1000;
  $(".coordonnée1").text(`▸ Lattitude: ${lat}°`);
  $(".coordonnée2").text(`▸ Longitude: ${long}°`);
  carte.flyTo([lat, long], 12);
  //L.marker([lat, long]).addTo(carte);
  //L.marker([lat, long], {icon: cinIcon}).addTo(carte);

  // Fonction qui crée tous les ping de tous les cinémas
  $.getJSON('script/cinema_fr.geojson', function (data) {
    L.geoJSON(data, {
        onEachFeature: function(feature, layer) {
            onEachFeature(feature, layer, lat, long);
        },
        filter: function(feature) {
            if (feature.geometry && feature.geometry.coordinates) {
                return (Math.abs(feature.geometry.coordinates[0] - long) <= 0.15 && 
                        Math.abs(feature.geometry.coordinates[1] - lat) <= 0.15);
            }
            return false;
        }
    }).addTo(carte);
  });

  // console.log(lat);
  // console.log(long);
}

// Fonction qui récupère juste les noms des cinémas si ils sont à moins de 10km
function onEachFeature(feature, layer, userLat, userLong) {
  // approximation de 10km en degrés - à ajuster selon vos besoins 
  
  if (feature.properties && feature.properties.NOM_ETABLISSEMENT) {
      if (Math.abs(feature.geometry.coordinates[0] - userLong) <= 0.15 && 
          Math.abs(feature.geometry.coordinates[1] - userLat) <= 0.15) {
          
          layer.bindPopup(feature.properties.NOM_ETABLISSEMENT).setIcon(cinIcon);
      } else {
          // Si le cinéma est hors de la portée, nous ne l'ajoutons pas à la carte.
          return null;
      }
  }
}


// Appelée si échec de récuparation des coordonnées
function positionErreur(erreurPosition) {
  // Cas d'usage du switch !
  let natureErreur;
  switch (erreurPosition.code) {
    case erreurPosition.TIMEOUT:
      // Attention, durée par défaut de récupération des coordonnées infini
      natureErreur = "La géolocalisation prends trop de temps...";
      break;
    case erreurPosition.PERMISSION_DENIED:
      natureErreur = "Vous n'avez pas autorisé la géolocalisation.";
      break;
    case erreurPosition.POSITION_UNAVAILABLE:
      natureErreur = "Votre position n'a pu être déterminée.";
      break;
    default:
      natureErreur = "Une erreur inattendue s'est produite.";
  }
  // Injection du texte
  $(".erreur").text(natureErreur);
}

//change l'apparence du pointeur au survol du texte click here !
document.querySelector(".coord").style.cursor = "pointer";


var cinIcon = L.icon({
  iconUrl: 'medias/favicon/1661309-200.png',

  iconSize:     [50, 50], // size of the icon
  iconAnchor:   [25, 50], // point of the icon which will correspond to marker's location
  popupAnchor:  [0, -50] // point from which the popup should open relative to the iconAnchor
});



// film  

fetch('script/cURL.php')
      .then(response => response.json())
      .then(json => castDisplay(json));
      // .then(json => console.log(json))

const sectionCast = document.querySelector("section:last-of-type>div>div");
// console.log(sectionCast);
function castDisplay(json){
  for( let i = 0; i < 15; i++) {
    let actorName = json['cast'][i]['name'];
    let actorImg = json['cast'][i]['profile_path'];
    let actorId = json['cast'][i]['id'];
    const div = document.createElement('div');
    div.classList.add("imgActor");
    const name = document.createElement('h3');
    name.textContent = actorName;
    div.appendChild(name);
    const img = document.createElement("img");
    img.setAttribute('src', 'https://image.tmdb.org/t/p/w300_and_h450_bestv2'+actorImg)
    div.appendChild(img);
    div.setAttribute('id', actorId)
    sectionCast.appendChild(div);
  }
  let actors = document.querySelectorAll('.imgActor');
  display($('.imgActor:first-of-type').attr('id'))
  actors.forEach(actor => { 
    actor.addEventListener('click', () =>{
      let id = actor.getAttribute('id');
      display(id);
    })
  });
}

function display(id){
  let id0bj = new FormData()
  id0bj.append('id', id)
  fetch('script/cURL2.php', { method: 'POST', body: id0bj })
  .then(response => response.json())
  .then(json => {
    // console.log(json);
    const pInfo = document.querySelector('.resumActor');
    $(pInfo).empty();
    const iPerso = document.createElement('div');
    const actorImg = document.createElement('img');
    const actorName = document.createElement('h2');
    const DoB = document.createElement('h3');
    const PoB = document.createElement('h4');
    const bio = document.createElement('p');
    // console.log(actorImg);
    actorImg.setAttribute('src', 'https://image.tmdb.org/t/p/w300_and_h450_bestv2'+ json['profile_path']);
    actorImg.setAttribute('alt', json['name']);
    actorImg.classList.add('profile');
    iPerso.classList.add('iPerso');
    actorName.classList.add('name');
    DoB.classList.add('DoB');
    PoB.classList.add('PoB');
    bio.classList.add('bio');
    actorName.textContent = json['name'];
    DoB.textContent = json['birthday'];
    PoB.textContent = json['place_of_birth'];
    bio.textContent = json['biography'];
    pInfo.appendChild(actorImg);
    iPerso.appendChild(actorName);
    iPerso.appendChild(DoB);
    iPerso.appendChild(PoB);
    pInfo.appendChild(iPerso);
    pInfo.appendChild(bio);
  })
}
