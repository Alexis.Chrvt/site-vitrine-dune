<?php
// Si vous souhaitez vérifier de l'activation de cURL
// phpinfo();
// La catégorie curl doit apparaitre avec cURL support : enabled

// cURL
// 1- initialisation: curl_init
$api = curl_init();

$id = $_POST['id'];
// 2- gestion des options: curl_setopt
curl_setopt_array($api, [
    CURLOPT_URL => "https://api.themoviedb.org/3/person/$id?api_key=ed8b6710ac72f903369159da26060045",
    CURLOPT_RETURNTRANSFER => true, // Retourner les données dans $data
    CURLOPT_TIMEOUT        => 2, // Temps d'attente maxi en secondes
]);
// ATTENTION, il se peut que vous ayez à résoudre un problème de certificat avec Wamp

// 3- execution de la requête: curl_exec
$data = curl_exec($api);
// Gestion des erreurs
if ($data === false || curl_getinfo($api, CURLINFO_HTTP_CODE !== 200)) {
    $data = null;
}

// 4- traitement du résultat
// Décodage du JSON permettant de le travailler en php
// $data = json_decode($data);

// 5- fermeture de la session: curl_close
echo($data);
curl_close($api);
